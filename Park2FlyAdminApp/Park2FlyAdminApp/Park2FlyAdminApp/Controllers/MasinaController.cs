﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Park2FlyAdminApp.Models;

namespace Park2FlyAdminApp.Controllers
{
    [CheckAuthorization]
    public class MasinaController : Controller
    {
        private MasinaDbContext db = new MasinaDbContext();

        // GET: Masina
        public ActionResult Index()
        {
            return View(db.Masini.ToList());
        }
         
        public ActionResult Contract(int? id)
        {

            return View(db.Masini.Where(m => m.ContractID == id).ToList());
        }

        // GET: Masina/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Masina masina = db.Masini.Find(id);
            if (masina == null)
            {
                return HttpNotFound();
            }
            return View(masina);
        }

        // GET: Masina/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Masina/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Marca,Model,NrInmatriculare,Cheie,ContractID")] Masina masina)
        {
            if (ModelState.IsValid)
            {
                db.Masini.Add(masina);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(masina);
        }

        // GET: Masina/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Masina masina = db.Masini.Find(id);
            if (masina == null)
            {
                return HttpNotFound();
            }
            return View(masina);
        }

        // POST: Masina/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Marca,Model,NrInmatriculare,Cheie,ContractID")] Masina masina)
        {
            if (ModelState.IsValid)
            {
                db.Entry(masina).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(masina);
        }

        // GET: Masina/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Masina masina = db.Masini.Find(id);
            if (masina == null)
            {
                return HttpNotFound();
            }
            return View(masina);
        }

        // POST: Masina/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Masina masina = db.Masini.Find(id);
            db.Masini.Remove(masina);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
