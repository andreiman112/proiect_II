﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Park2FlyAdminApp.Models;

namespace Park2FlyAdminApp.Controllers
{
    [CheckAuthorization]
    public class AdresaController : Controller
    {
        private AdresaDbContext db = new AdresaDbContext();

        // GET: Adresa
        public ActionResult Index()
        {
            return View(db.Adrese.ToList());
        }

        // GET: Adresa/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adresa adresa = db.Adrese.Find(id);
            if (adresa == null)
            {
                return HttpNotFound();
            }
            return View(adresa);
        }

        // GET: Adresa/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Adresa/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Localitate,Strada,Numar,Bloc,Scara,Etaj,Apartament,Judet")] Adresa adresa)
        {
            if (ModelState.IsValid)
            {
                db.Adrese.Add(adresa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(adresa);
        }

        // GET: Adresa/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adresa adresa = db.Adrese.Find(id);
            if (adresa == null)
            {
                return HttpNotFound();
            }
            return View(adresa);
        }

        // POST: Adresa/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Localitate,Strada,Numar,Bloc,Scara,Etaj,Apartament,Judet")] Adresa adresa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(adresa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index","Contract");
            }
            return View(adresa);
        }

        // GET: Adresa/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adresa adresa = db.Adrese.Find(id);
            if (adresa == null)
            {
                return HttpNotFound();
            }
            return View(adresa);
        }

        // POST: Adresa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Adresa adresa = db.Adrese.Find(id);
            db.Adrese.Remove(adresa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
