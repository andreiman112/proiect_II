﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Park2FlyAdminApp.Models;

namespace Park2FlyAdminApp.Controllers
{
    [CheckAuthorization]
    public class ContractController : Controller
    {
        private ContractDbContext dbContract = new ContractDbContext();
        private ClientDbContext dbClient = new ClientDbContext();
        private AdresaDbContext dbAdresa = new AdresaDbContext();
        private FirmaDbContext dbFirma = new FirmaDbContext();
        private MasinaDbContext masinaDb = new MasinaDbContext();

        // GET: Contract
        public ActionResult Index()
        {
          
            return View(dbContract.Contracte.ToList());
        }

        // GET: Contract
        public ActionResult Plecari(string dataFinal)
        {
            DateTime data;
            if (dataFinal!=null && dataFinal != "")
                data = DateTime.Parse(dataFinal);
            else data = DateTime.Now;
            return View(dbContract.Contracte.Where(m => DbFunctions.DiffDays(m.DataFinal, data) == 0).ToList());
        }

        // GET: Contract/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contract contract = dbContract.Contracte.Find(id);
            if (contract == null)
            {
                return HttpNotFound();
            }
            return View(contract);
        }

        // GET: Contract/Create
        Contract c1 = new Contract();
        
        public ActionResult Create()
        {
            Park2FlyAdminApp.Models.ClientDbContext clientDbContext = new ClientDbContext();
            ViewData["listaClienti"] = clientDbContext.Clienti.OrderBy(l => l.NumeClient).ToList();

            Park2FlyAdminApp.Models.LocatieDbContext locatieDbContext = new LocatieDbContext();
            ViewData["listaLocatii"] = locatieDbContext.Locatii.OrderBy(l=>l.Nume).ToList();
            //   c1.DataFinal = DateTime.Now;
            Contract c1 = new Contract();

           // c1.client = dbClient.Clienti.Find(1);
            //c1.ID=102;
            return View(c1);
        }

        // POST: Contract/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,DataStart,DataFinal,Pret,LocatieID,ClientID")] Contract contract)
        public ActionResult Create(FormCollection formCollection)
        {
            try
            {

                Contract contract = new Contract();
                Client client = new Client();
                //ClientDbContext clientDbContext = new ClientDbContext();
                //clientDbContext.Clienti.First(c => c.Numar == "");
                #region get formcollection data
                int idClient = formCollection["IDClient"].Length > 0 ? int.Parse(formCollection["IDClient"]) : 0;

                string numeClient = formCollection["NumeClient"];
                string buletin = formCollection["Buletin"];
                string cnp = formCollection["CNP"];
                string email = formCollection["Email"];
                string telefon = formCollection["client.Telefon"];
                string localitate = formCollection["Localitate"];
                string strada = formCollection["Strada"];
                string numar = formCollection["Numar"];
                string bloc = formCollection["Bloc"];
                string scara = formCollection["Scara"];
                int etaj = formCollection["Etaj"].Length > 0 ? int.Parse(formCollection["Etaj"]) : 0;
                int apartament = formCollection["Apartament"].Length > 0 ? int.Parse(formCollection["Apartament"]) : 0;
                string judet = formCollection["Judet"];


                int idFirma = formCollection["IDFirma"].Length > 0 ? int.Parse(formCollection["IDFirma"]) : 0;
                string numeFirma = formCollection["NumeFirma"];
                string cui = formCollection["CUI"];
                string j = formCollection["J"];
                string cont = formCollection["Cont"];
                string localitateF = formCollection["LocalitateF"];
                string stradaF = formCollection["StradaF"];
                string numarF = formCollection["NumarF"];
                string blocF = formCollection["BlocF"];
                string scaraF = formCollection["ScaraF"];
                int etajF = formCollection["EtajF"].Length > 0 ? int.Parse(formCollection["EtajF"]) : 0;
                int apartamentF = formCollection["ApartamentF"].Length > 0 ? int.Parse(formCollection["ApartamentF"]) : 0;
                string judetF = formCollection["JudetF"];



                #endregion
                if (idClient == 0) //client nou, adresa noua
                {
                    Adresa adresa = new Adresa();
                    adresa.Apartament = apartament;
                    adresa.Bloc = bloc;
                    adresa.Etaj = etaj;
                    adresa.Judet = judet;
                    adresa.Localitate = localitate;
                    adresa.Numar = numar;
                    adresa.Scara = scara;
                    adresa.Strada = strada;

                    dbAdresa.Adrese.Add(adresa);
                    dbAdresa.SaveChanges();


                    client.AdresaID = adresa.ID;
                    client.Buletin = buletin;
                    client.CNP = cnp;
                    client.Email = email;
                    client.NumeClient = numeClient;


                    dbClient.Clienti.Add(client);
                    dbClient.SaveChanges();
                    idClient = client.ID;

                }

                contract.ClientID = idClient;
                client = dbClient.Clienti.Find(idClient);
                if (client.Telefon != telefon)
                {
                    client.Telefon = telefon;
                    dbClient.SaveChanges();

                }


                if (numeFirma != "" && idFirma == 0)//firma noua(nexistenta in DB), adresa noua
                {
                    Adresa adresa = new Adresa();
                    adresa.Apartament = apartamentF;
                    adresa.Bloc = blocF;
                    adresa.Etaj = etajF;
                    adresa.Judet = judetF;
                    adresa.Localitate = localitateF;
                    adresa.Numar = numarF;
                    adresa.Scara = scaraF;
                    adresa.Strada = stradaF;

                    dbAdresa.Adrese.Add(adresa);
                    dbAdresa.SaveChanges();

                    Firma firma = new Firma();
                    firma.Cont = cont;
                    firma.CUI = cui;
                    firma.J = j;
                    firma.NumeCompanie = numeFirma;
                    firma.AdresaID = adresa.ID;
                    dbFirma.Firme.Add(firma);
                    dbFirma.SaveChanges();
                    idFirma = firma.ID;
                }

                contract.FirmaID = idFirma;




                contract.DataFinal = DateTime.Parse(formCollection["DataFinal"]);
                contract.DataFinal += TimeSpan.Parse(formCollection["TimpFinal"]);
                contract.DataStart = DateTime.Parse(formCollection["DataStart"]);
                contract.DataStart += TimeSpan.Parse(formCollection["TimpStart"]);
                contract.LocatieID = Int16.Parse(formCollection["LocatieID"]);
                contract.Pret = Int16.Parse(formCollection["Pret"]);



                dbContract.Contracte.Add(contract);
                dbContract.SaveChanges();


                string[] marca = formCollection["marcaMasina"].Split(',');
                string[] model = formCollection["modelMasina"].Split(',');
                string[] numar_ = formCollection["numarMasina"].Split(',');
                for (int i = 0; i < marca.Length; i++)
                {
                    Masina m = new Masina();
                    m.Marca = marca[i];
                    m.Model = model[i];
                    m.NrInmatriculare = numar_[i];
                    m.ContractID = contract.ID;


                    masinaDb.Masini.Add(m);

                }
                masinaDb.SaveChanges();
                return RedirectToAction("Index");

               }
            catch (Exception ex)
            {
                return RedirectToAction(ex.Message);
            }
        }

        // GET: Contract/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contract contract = dbContract.Contracte.Find(id);
            if (contract == null)
            {
                return HttpNotFound();
            }
            return View(contract);
        }

        // POST: Contract/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Data,DataStart,DataFinal,Pret,LocatieID,ClientID")] Contract contract)
        {
            if (ModelState.IsValid)
            {
                dbContract.Entry(contract).State = EntityState.Modified;
                dbContract.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contract);
        }

        // GET: Contract/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contract contract = dbContract.Contracte.Find(id);
            if (contract == null)
            {
                return HttpNotFound();
            }
            return View(contract);
        }
        public ActionResult ViewFirmaNoua()
        {

            return PartialView("_FirmaNoua");
        }
        public ActionResult ViewClientNou()
        {
           //c1.DataFinal = System.DateTime.Now;
            return PartialView("_ClientNou");
        }
        public ActionResult ViewSelectFirma()
        {

            return PartialView("_SelectFirma", dbFirma.Firme.ToList());
        }
        public ActionResult ViewSelectareClient()
        {
            //c1.DataFinal = System.DateTime.Now;
            return PartialView("_SelectareClient", dbClient.Clienti.ToList());
        }
        Client c=new Client();
      
        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public void ClientNou(Client client)
        {
            if (ModelState.IsValid)
            {
                dbAdresa.Adrese.Add(client.Adresa);
                dbAdresa.SaveChanges();
                client.AdresaID = client.Adresa.ID;
               
                dbClient.Clienti.Add(client);
                dbClient.SaveChanges();

                Contract c = new Contract();
                c.client = client;
           //     return View("Create",c);
            }
            //return PartialView("_ClientNou");

        }

        // POST: Contract/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contract contract = dbContract.Contracte.Find(id);
            dbContract.Contracte.Remove(contract);
            dbContract.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbContract.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
