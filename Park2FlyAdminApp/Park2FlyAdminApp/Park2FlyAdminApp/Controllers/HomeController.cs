﻿using Park2FlyAdminApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Park2FlyAdminApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index( )
        {
            return RedirectToAction("Index", "Contract");
        }
        public ActionResult Index1(int? id)
        {
            ViewBag.Message = "Your application description page." + id;
            Park2FlyAdminApp.Models.ClientDbContext clientDbContext = new Models.ClientDbContext();
            for(int i=0;i<1000;i++)
            {
                Park2FlyAdminApp.Models.Client client = new Models.Client();
                client.NumeClient = "Client " + i;
                //client.Etaj = i;
                //client.Apartament = i;
                clientDbContext.Clienti.Add(client);
            }
            clientDbContext.SaveChanges();

            return View();
        }
        [CheckAuthorization]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpGet]
        public ActionResult Login(string returnURL)
        {
            var userinfo = new User();

            try
            {
                // We do not want to use any existing identity information
                //EnsureLoggedOut();

                // Store the originating URL so we can attach it to a form field
                //userinfo.ReturnURL = returnURL;

                return View(userinfo);
            }
            catch
            {
                throw;
            }
        }
        
        public ActionResult Logout()
        {
            Session["UserID"] = null;
            return RedirectToAction("Login", "Home");

        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User entity)
        {
            bool isLogin = true;
            UserDbContext userDbContext = new UserDbContext();
            User user= userDbContext.Users.ToList().Find(u => u.ID == entity.ID && u.Password == entity.Password);
           
            if (user!=null)
                    {
                    
                    FormsAuthentication.SignOut();
                   
                    FormsAuthentication.SetAuthCookie(user.ID, true);
                    
                    Session["UserID"] = user.ID;
                 
                    return RedirectToAction("Index");
                    }
                    else
                    { 
                        return View(entity);
                    }

        }
    }
}