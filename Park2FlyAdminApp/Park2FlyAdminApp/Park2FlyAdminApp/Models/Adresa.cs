﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Park2FlyAdminApp.Models
{
    public class Adresa
    {
        public int ID { get; set; }
        [Required()]
        public string Localitate { get; set; }
        public string Strada { get; set; }
        public string Numar { get; set; }
        public string Bloc { get; set; }

        public string Scara { get; set; }
        public int Etaj { get; set; }
        [Required()]
        public int Apartament { get; set; }
        public string Judet { get; set; }
    }
    public class AdresaDbContext:DbContext
    {
        public DbSet<Adresa> Adrese { get; set; }
        public AdresaDbContext():base("name=DbContextConnStr")
        {

        }
    }
}