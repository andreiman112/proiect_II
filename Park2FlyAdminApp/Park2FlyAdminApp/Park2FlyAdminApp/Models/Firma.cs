﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Park2FlyAdminApp.Models
{
    public class Firma
    {
        public int ID { get; set; }

        public string NumeCompanie { get; set; }
        public string Cont { get; set; }
        public string CUI { get; set; }
        public string J { get; set; }

        public int AdresaID { get; set; }
        
        [NotMapped]
        public Adresa Adresa { get; set; }

    }
    public class FirmaDbContext : DbContext
    {
        public DbSet<Firma> Firme { get; set; }
        public FirmaDbContext() : base("name=DbContextConnStr")
        {

        }

        public System.Data.Entity.DbSet<Park2FlyAdminApp.Models.Contract> Contracts { get; set; }

        public System.Data.Entity.DbSet<Park2FlyAdminApp.Models.User> Users { get; set; }
    }
}