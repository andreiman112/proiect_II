﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Park2FlyAdminApp.Models
{
    public class Locatie
    {
        public int ID { get; set; }
        [Required()]
        public string Nume { get; set; }
    }

    public class LocatieDbContext : DbContext
    {
        public System.Data.Entity.DbSet<Park2FlyAdminApp.Models.Locatie> Locatii { get; set; }
        public LocatieDbContext()
        {
            this.Database.Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DbContextConnStr"].ConnectionString;
        }

     
    }
}