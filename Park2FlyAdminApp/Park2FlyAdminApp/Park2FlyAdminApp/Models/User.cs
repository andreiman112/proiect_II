﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Park2FlyAdminApp.Models
{
    public class User
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
    }
    public class UserDbContext : DbContext
    {
        public System.Data.Entity.DbSet<Park2FlyAdminApp.Models.User> Users { get; set; }
        public UserDbContext()
        {
            this.Database.Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DbContextConnStr"].ConnectionString;
        }


    }
}