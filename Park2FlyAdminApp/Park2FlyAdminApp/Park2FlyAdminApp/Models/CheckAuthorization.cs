﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Park2FlyAdminApp.Models
{
    public class CheckAuthorization : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
          
            if (HttpContext.Current.Session["UserID"] == null)
            {
                      filterContext.Result = new RedirectResult("~/Home/Login");
                      filterContext.HttpContext.Server.UrlEncode(filterContext.HttpContext.Request.RawUrl);
                
            }

        }

    }
}