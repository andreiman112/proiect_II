﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Park2FlyAdminApp.Models
{
    public class Contract
    {
        public int ID { get; set; }
         
        [DataType(DataType.Date)]
        public DateTime DataStart { get; set; }
        [DataType(DataType.Date)]
        public DateTime DataFinal { get; set; }
        public int Pret { get; set; }
        [Display(Name = "Locatie")]
        [Required()]
        public int LocatieID { get; set; }
        public int ClientID { get; set; }
        public int FirmaID { get; set; }
        [NotMapped]
        public Client client { get; set; }
        [NotMapped]
        [DataType(DataType.Time)]
        public DateTime TimpStart { get; set; }
        [NotMapped]
        [DataType(DataType.Time)]
        public DateTime TimpFinal { get; set; }
        public Contract()
        {
            
        }
    }
    public class ContractDbContext : DbContext
    {
        public DbSet<Contract> Contracte { get; set; }
        public ContractDbContext()
        {
            this.Database.Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DbContextConnStr"].ConnectionString;
        }

    }
}