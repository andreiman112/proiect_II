﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Park2FlyAdminApp.Models
{
    public  class Client
    {
        public int ID { get; set; }
      
 
        public string NumeClient { get; set; }
        public string Buletin { get; set; }
        public string CNP { get; set; }
        public string Email { get; set; }

        public int AdresaID { get; set; }

        public string Telefon { get; set; }

        [NotMapped]
        public Adresa Adresa { get; set; }
    }
    public class ClientDbContext : DbContext
    {
        public DbSet<Client> Clienti { get; set; }
        public ClientDbContext()
        {
            this.Database.Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DbContextConnStr"].ConnectionString;
        }

    }
}