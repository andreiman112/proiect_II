﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace Park2FlyAdminApp.Models
{
    public class Masina
    {
        //[Key]
        public int ID { get; set; }
        [Required()]
        [StringLength(int.MaxValue, MinimumLength = 1)]
        public string Marca { get; set; }
        [Required()]
        [StringLength(int.MaxValue, MinimumLength = 1)]
        public string Model { get; set; }
        [Required()]
        [StringLength(int.MaxValue, MinimumLength = 3)]
        public string NrInmatriculare { get; set; }
        
        public bool Cheie { get; set; }
        [Required()]
        public int ContractID { get; set; }
    }
    public class MasinaDbContext : DbContext
    {
        public DbSet<Masina> Masini { get; set; }
        public MasinaDbContext()
        {
            this.Database.Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DbContextConnStr"].ConnectionString;
        }
 
    }
}