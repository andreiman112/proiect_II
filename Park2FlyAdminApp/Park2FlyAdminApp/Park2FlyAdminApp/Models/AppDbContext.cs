﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Park2FlyAdminApp.Models
{
    public class AppDbContext:DbContext
    {
        public AppDbContext()
        {
            this.Database.Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DbContextConnString"].ConnectionString;
        }
    }
}