﻿using Park2FlyAdminApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomHelpers
{
    public static class DisplayHelper
    {
      
        public static MvcHtmlString DisplayClient(int id)
        {
            ClientDbContext dbClient = new ClientDbContext();
            Client c = dbClient.Clienti.ToList().Find(cl => cl.ID == id);
            if (c == null) return new MvcHtmlString("");
            return new MvcHtmlString(String.Format("<a href='client/Details/{0}'>{1}</a>",id,c.NumeClient));

        }

        public static string DisplayLocatie(int id)
        {
            LocatieDbContext dbContext = new LocatieDbContext();
            Locatie l = dbContext.Locatii.ToList().Find(entity => entity.ID == id);
                if (l == null) return "";
            return  l.Nume;

        }
         
        public static MvcHtmlString DisplayFirma(int id)
        {
            if (id == 0) return new MvcHtmlString("-");
            FirmaDbContext dbContext = new FirmaDbContext();
            return new MvcHtmlString(String.Format("<a href='firma/Details/{0}'>{1}</a>", id, dbContext.Firme.ToList().Find(entity => entity.ID == id).NumeCompanie));

        }
        public static MvcHtmlString DisplayAdresa(int id)
        {
            if (id == 0) return new MvcHtmlString("-"); 
            return new MvcHtmlString(String.Format("<a href='/Adresa/Details/{0}'>Vizualizare adresa</a>", id));

        }
    }
}